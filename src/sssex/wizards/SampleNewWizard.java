package sssex.wizards;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.core.runtime.*;
import org.eclipse.jface.operation.*;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.core.resources.*;
import org.eclipse.core.runtime.CoreException;

import java.io.*;

import org.eclipse.ui.*;
import org.eclipse.ui.ide.IDE;

/**
 * This is a sample new wizard. Its role is to create a new file 
 * resource in the provided container. If the container resource
 * (a folder or a project) is selected in the workspace 
 * when the wizard is opened, it will accept it as the target
 * container. The wizard creates one file with the extension
 * "eclick". If a sample multi-page editor (also available
 * as a template) is registered for the same extension, it will
 * be able to open it.
 */

public class SampleNewWizard extends Wizard implements INewWizard {
	private SampleNewWizardPage page;
	private ISelection selection;

	/**
	 * Constructor for SampleNewWizard.
	 */
	public SampleNewWizard() {
		super();
		setNeedsProgressMonitor(true);
	}
	
	/**
	 * Adding the page to the wizard.
	 */

	public void addPages() {
		page = new SampleNewWizardPage(selection);
		addPage(page);
	}

	/**
	 * This method is called when 'Finish' button is pressed in
	 * the wizard. We will create an operation and run it
	 * using wizard as execution context.
	 */
	public boolean performFinish() {
		final String containerName = page.getContainerName();
		final String fileName = page.getFileName();
		IRunnableWithProgress op = new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor) throws InvocationTargetException {
				try {
					doFinish(containerName, fileName, monitor);
				} catch (CoreException e) {
					throw new InvocationTargetException(e);
				} finally {
					monitor.done();
				}
			}
		};
		try {
			getContainer().run(true, false, op);
		} catch (InterruptedException e) {
			return false;
		} catch (InvocationTargetException e) {
			Throwable realException = e.getTargetException();
			MessageDialog.openError(getShell(), "Error", realException.getMessage());
			return false;
		}
		return true;
	}
	
	/**
	 * The worker method. It will find the container, create the
	 * file if missing or just replace its contents, and open
	 * the editor on the newly created file.
	 */

	private void doFinish(
		String containerName,
		String fileName,
		IProgressMonitor monitor)
		throws CoreException {
		// create a sample file
		monitor.beginTask("Creating " + fileName, 2);
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IResource resource = root.findMember(new Path(containerName));
		if (!resource.exists() || !(resource instanceof IContainer)) {
			throwCoreException("Container \"" + containerName + "\" does not exist.");
		}
		IContainer container = (IContainer) resource;
		final IFile file = container.getFile(new Path(fileName));
		try {
			InputStream stream = openContentStream(containerName);
			if (file.exists()) {
				file.setContents(stream, true, true, monitor);
			} else {
				file.create(stream, true, monitor);
			}
			stream.close();
		} catch (IOException e) {
		}
		monitor.worked(1);
		monitor.setTaskName("Opening file for editing...");
		getShell().getDisplay().asyncExec(new Runnable() {
			public void run() {
				IWorkbenchPage page =
					PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				try {
					IDE.openEditor(page, file, true);
				} catch (PartInitException e) {
				}
			}
		});
		monitor.worked(1);
	}
	
	/**
	 * We will initialize file contents with a sample text.
	 */

	private InputStream openContentStream(String ipath) throws UnsupportedEncodingException {
		String contents =
		
		"<html>\n"+
		"<head><meta charset=\"UTF-8\"></head>\n"+
		"<body>\n" +
		"	此插件诞生的意义在于，让程序员们在项目中将经常搜索的\"关键字\"得以保存在一个\"文件\"中，<br/>" +
		"	并且整个开发团队都可以共享，让程序开发不再冰冷，让资源和经验的分享变得火热，让新员工不再啰嗦地去问.<BR/>"+
		"	搜索超链接API:\n"+
		"	<table style=\"width:80%;\" border=\"1\">\n"+
		"		<tr>\n"+
		"			<td width=\"20%\">scope</td>\n"+
		"			<td width=\"80%\">\n"+
		"				搜索范围,多个范围以分号分隔,例如\"\\sss\\src;\\twoj;/eclick/src/sssex/Activator.java\"\n"+
		"				其中路径符号\"/\"和\"\\\"无所谓可以混合使用也可以使用一种,即可以\\sss/src也可以/sss/src也可以\\sss\\src\n"+
		"				表示将依次搜索sss项目下src目录中的所有文件，以及twoj项目，以及eclick项目中的Activator.java类文件\n"+
		"			</td>\n"+
		"		</tr>\n"+
		"		<tr>\n"+
		"			<td>string</td>\n"+
		"			<td>搜索字符串,不能包含与超链接冲突的字符例如\"?\",\"=\"</td>\n"+
		"		</tr>\n"+
		"		<tr>\n"+
		"			<td>搜索服务器端口</td>\n"+
		"			<td>目前版本为固定值\"2016\"</td>\n"+
		"		</tr>\n"+
		"		<tr>\n"+
		"			<td colspan=\"2\">\n"+
		"				&nbsp;例如：&lt;a href=\"http://localhost:2016/?scope=\\eclick\\src&string=import\" target=\"noframe\"&gt;import&lt;/a&gt;<BR/>\n"+
		"				这个超链接表示在eclick项目的src目录中搜索所有文件中的import字符串\n"+
		"			</td>\n"+
		"		</tr>\n"+
		"	</table>\n"+
		"	<span>OSC地址：</span><a href=\"http://www.oschina.net/p/eclick\" target=\"_blank\">http://www.oschina.net/p/eclick</a>\n"+
		"	<br/>\n"+
		"	<span>API地址：</span><a href=\"http://www.ifelsefor.cc/?tab=2\" target=\"_blank\">http://www.ifelsefor.cc/?tab=2</a>\n"+
		"	\n"+
		"	\n"+
		"	\n"+
		"	<br/>\n"+
		"	<br/>\n"+
		"	<!-- 影藏iframe的存在是为了让超链接不产生跳转，保持点击超链接始终停留在本页面  -->\n"+
		"	<iframe style=\"display:none;\" name=\"noframe\"></iframe>\n"+
		"	<span>搜索关键字demo：</span>\n"+
		"	<br/>\n"+
		"	<a href=\"http://localhost:2016/?scope="+ipath+"&string=package\" target=\"noframe\" title=\"点击我将在["+ipath+"]等目录中搜索[package]字符串\">package</a>\n"+
		"	<a href=\"http://localhost:2016/?scope="+ipath+"&string=import\" target=\"noframe\" title=\"点击我将在["+ipath+"]等目录中搜索[import]字符串\">import</a>\n"+
		"	<a href=\"http://localhost:2016/?scope="+ipath+"&string=new\" target=\"noframe\" title=\"点击我将在["+ipath+"]等目录中搜索[new]字符串\">new</a>\n"+
		"	<a href=\"http://localhost:2016/?scope="+ipath+"&string=class\" target=\"noframe\" title=\"点击我将在["+ipath+"]等目录中搜索[class]字符串\">class</a>\n"+
		"	\n"+
		"	\n"+
		"</body>\n"+
	"</html>";
		
		return new ByteArrayInputStream(contents.getBytes("UTF-8"));
	}

	private void throwCoreException(String message) throws CoreException {
		IStatus status =
			new Status(IStatus.ERROR, "sssex", IStatus.OK, message, null);
		throw new CoreException(status);
	}

	/**
	 * We will accept the selection in the workbench to see if
	 * we can initialize from it.
	 * @see IWorkbenchWizard#init(IWorkbench, IStructuredSelection)
	 */
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.selection = selection;
	}
}